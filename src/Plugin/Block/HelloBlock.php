<?php
namespace Drupal\days_left_event\Plugin\Block;
use Drupal\Core\Block\BlockBase;



/**
 *
 *
 * @Block(
 *   id = "days_left_event",
 *   admin_label = @Translation("Days Left Event"),
 *   category = @Translation("Events"),
 * )
 */

class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function current_node(){
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $nid = $node->id();
    }else{
      $nid = 0;
    }
    return $nid;
  }
  public function GetEventStartDate($id){
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
    $start_date = $node->get('field_date')->getValue();
    $start_date = $start_date[0]['value'];
    $start_date = strtotime($start_date);//UNIX
    return $start_date;
  }
  public function DaysTillEventStart($start_date)//TAKE UNIX AS START DATE
  {
    $today = time();//NOW
    $till_event = $start_date - $today;
    $days_till_event=round($till_event/(60*60*24));
    //CHECK IF EVENT IS ALREADY STARTED/ENDED
    if($days_till_event > 0){
      return sprintf("%s days left until event starts.", $days_till_event);

    }elseif ($days_till_event == 0) {
      return sprintf("This event is happening today!");
      }
    else{
        return sprintf("Event finished %s days ago", abs($days_till_event));
      }
    }
    private function GetRoute(){
      $route = \Drupal::routeMatch()->getRouteName();
      return $route;
    }
    private function GetAllEvents(){
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'event')
      ->condition('status', 1)
      ->sort('field_date', 'ASC');
      $nids = $query->execute();
      return $nids;
    }

  public function getCacheMaxAge() {
    return 0;
  }
  public function build() {
    $route=$this->GetRoute();
    if($route=='entity.node.canonical')//WHEN VIEWING NODE
    {
      return [
        '#markup' => $this->DaysTillEventStart($this->GetEventStartDate($this->current_node())),
      ];
    }
    else // OTHER PAGES-> Get all events and show first five
    {
      $events=$this->GetAllEvents();
      $output='';
      $dates=array();
      foreach($events as $event){
        $event_date=$this->GetEventStartDate($event);
        $dates[$event]=$event_date;
      }
      arsort($dates);//SORT BY DATE DESC
      foreach($dates as $event=>$date){
        $event_name=\Drupal::entityTypeManager()->getStorage('node')->load($event)->getTitle();
        $output.=$event_name.': '.$this->DaysTillEventStart($date).'<br>';
      }
      return [
        '#markup' => $output,
      ];
    }


  }

}
